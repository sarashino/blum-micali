#[macro_use]
extern crate text_io;

static p: u32 = 11;
static g: u32 = 2;
fn main() {
    loop {
        println!("length: ");
        let len: usize = read!();
        let acc = &mut Vec::new();
        for x in f(len-1, 0,1,acc) {
            print!("{},", x);
        }
        println!("");
    }
}
fn f(len: usize, l: usize, x: u32, acc: &mut Vec<u32>) -> &Vec<u32> {
    let x = g.pow(x) % p;
    acc.push(x);
    if len == l {
        return &*acc;
    } else {
        return f(len, l + 1, x, acc);
    }
}
